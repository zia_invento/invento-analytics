# Laravel Invento Analytics

Compatible  with only Laravel multi-purpose CMS 



1. Install the package via Composer:

    ```sh
     composer require invento/analytics
    ```

    The package will automatically register its service provider.

2. Optionally, publish the configuration file if you want to change any defaults:

    ```sh
    php artisan vendor:publish --provider="Invento\Analytics\InventoAnalyticServiceProvider"
    ```


## You can publish separately

1. Publish the configuration file if you want to change any defaults:

    ```sh
    php artisan vendor:publish --provider="Invento\Analytics\InventoAnalyticServiceProvider" --tag="analytics-config"
    ```

2. Publish the view file if you want to change any defaults:

    ```sh
    php artisan vendor:publish --provider="Invento\Analytics\InventoAnalyticServiceProvider" --tag="analytics-views"
    ```


3. Publish the lang file if you want to change any defaults:

    ```sh
    php artisan vendor:publish --provider="Invento\Analytics\InventoAnalyticServiceProvider" --tag="analytics-lang"
    ```


4. At last clear cache and run autoload:

    ```sh
   php artisan optimize
   composer dump-autoload
    ```


## Copyright and License

[invento-analytics](https://bitbucket.org/zia_invento/invento-analytics/src/master/)
was written by [Awlad Hossain] and is released under the 
[MIT License](LICENSE.md).

Copyright (c) 2024 Invento
