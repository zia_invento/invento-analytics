<?php

return [
    'invento_analytics' => 'Invento Analytics',
    'analytics_configuration' => 'Analytics Configuration',
    'property_id' => 'Property ID',
    'property_id_text' => 'Enter google analytics Property ID',
    'service_account_key_file' => 'Service account key file',
    'service_account_key_file_text' => 'Rename the file name as "service_account_key.json"',
    'status' => 'Status',
    'analytics_credentials' => 'Analytics Credentials',
    'submit' => 'Submit',
    'configuration_updated' => 'Configuration updated successfully',

];