<x-default-layout>

    @section('browser_title') {{ __('analytics::analytics.invento_analytics') }} @endsection

    @section('title')
        {{ __('analytics::analytics.analytics_configuration') }}
    @endsection

    @section('breadcrumbs')
        {{ Breadcrumbs::render('analytics.config') }}
    @endsection

    <div class="card card-flush">
        <div class="card-header">
            <div class="card-title">
                <h4>{{ __('analytics::analytics.analytics_credentials') }}</h4>
            </div>
        </div>

        <div class="card-body py-4" id="analytics_config">

            <form action="{{ route('admin.packages.config-analytics.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="mb-10 fv-row form-group">
                    <label class="required form-label">{{ __('analytics::analytics.property_id') }}</label>
                    <input required type="text" name="property_id" id="property_id" class="form-control mb-2"
                           value="{{ old('property_id',$property_id) }}"/>

                    <div class="text-muted fs-7">{{ __('analytics::analytics.property_id_text') }}</div>

                </div>

                <div class="mb-10 fv-row form-group">
                    <label class="required form-label">{{ __('analytics::analytics.service_account_key_file') }}</label>
                    <input type="file" name="key" id="key" class="form-control mb-2 "/>

                    <div class="text-muted fs-7">{{ __('analytics::analytics.service_account_key_file_text') }}</div>

                </div>

                <div class="mb-10">
                    <label class="form-check form-switch form-check-custom form-check-solid">
                        <input class="form-check-input" name="status" type="checkbox" {{(  isset($status ) && $status) ? "checked"  : ''  }} />
                        <span class="form-check-label fw-semibold text-muted">{{ __('analytics::analytics.status') }}</span>
                    </label>
                </div>

                <div class="d-flex justify-content-end mb-4">
                    <button type="submit" class="btn btn-info"><span class="indicator-label">{{ __('analytics::analytics.submit') }}</span></button>
                </div>

            </form>

        </div>
    </div>


</x-default-layout>

