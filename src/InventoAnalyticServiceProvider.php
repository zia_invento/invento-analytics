<?php

namespace Invento\Analytics;

use Illuminate\Support\ServiceProvider;
use Spatie\Analytics\Analytics;
use Spatie\Analytics\AnalyticsClientFactory;


class InventoAnalyticServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(\Diglactic\Breadcrumbs\ServiceProvider::class);

        $this->app->singleton(Analytics::class, function ($app) {
            $analyticsConfig = $app['config']['analytics'];
            $client = AnalyticsClientFactory::createForConfig($analyticsConfig);

            if (empty($analyticsConfig['property_id'])) {
                throw new \RuntimeException('Google Analytics view_id is not configured.');
            }

            return new Analytics($client, $analyticsConfig['property_id']);
        });

    }

    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/analytics.php', 'analytics');

        $this->publishes([
            __DIR__.'/../config/analytics.php' => config_path('analytics.php')
        ],'analytics-config');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'analytics');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/analytics')
        ],'analytics-views');

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'analytics');

        $this->publishes([
            __DIR__.'/../resources/lang' => $this->app->langPath('vendor/analytics')
        ],'analytics-lang');

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

//        $this->loadRoutesFrom(__DIR__.'/../routes/breadcrumbs.php');
    }
}