<?php

namespace Invento\Analytics\Controllers;

use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Valuestore\Valuestore;

class AnalyticController extends Controller
{

    public function index(){
        $this->store = Valuestore::make(resource_path('settings/settings.json'));

        $data['status'] = $this->store->has('analytics') ? $this->store->get('analytics')['status'] : '';
        $data['property_id'] = $this->store->has('analytics') ? $this->store->get('analytics')['property_id'] : '';

        return view('analytics::config',$data);
    }

    public function store(Request $request){

        $this->store = Valuestore::make(resource_path('settings/settings.json'));

        $status = $request->has('status');
        $property_id = $request->property_id;

        $data['analytics'] = [
            'status' => $status,
            'property_id' => $property_id
        ];

        if ($request->hasFile('key')) {
            $file = $request->file('key');
            $directory = 'analytics';
            $filename = $file->getClientOriginalName();

            // Check if the directory exists and create it if not
            if (!Storage::disk('local')->exists($directory)) {
                Storage::disk('local')->makeDirectory($directory);
            }

            Storage::disk('local')->putFileAs($directory, $file, $filename);
        }


        $this->store->put($data);

        Toastr::success(__('analytics::analytics.configuration_updated'),__('analytics::analytics.analytics_configuration'));

        return back();
    }
}