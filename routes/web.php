<?php

use Illuminate\Support\Facades\Route;
use Invento\Analytics\Controllers\AnalyticController;

Route::middleware(['web','auth'])->group(function () {
    Route::get('admin/packages/config-analytics', [AnalyticController::class, 'index'])->name('admin.packages.config-analytics');

    Route::post('admin/packages/config-analytics/store', [AnalyticController::class, 'store'])->name('admin.packages.config-analytics.store');
});
